"use strict";

const fs = require("fs");

function a() {
	try {
		fs.readdirSync("./logs");
	} catch (e) {
		fs.mkdirSync("./logs");
		fs.writeFileSync("./logs/crash.log", "");
		fs.writeFileSync("./logs/usage.log", "");
		console.warn("Missing Logs Directory");
	}
}

module.exports = a;