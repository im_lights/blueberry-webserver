"use strict";

// TODO: Optimize for best runtime speed

function run(f, session) {

	const files = sortFiles(f);

	// Setup our code strings
	let indexhtml = "<html>\n";
	let indexcss = "";
	let indexjs = "";

	// Setup HTML Head
	indexhtml += generateHTMLHead(files.html);

	indexhtml += "<body>\n<div id=\"wrapper\">\n";

	// Generate page header
	const header = generateHeader(files, session);
	indexhtml += header.html;
	indexcss += header.css;
	indexjs += header.js;

	indexhtml += "</div>\n</body>\n</html>\n"; // Close Body from Wrapper
	return returnFiles(indexhtml, indexcss, indexjs);
}

function sortFiles(f) {
	let files = {html: "", css: "", js: ""};
	for (let i = 0; i < f.length; i++) {
		switch(f[i][0]) {
		case "html": files.html = f[i][1]; break;
		case "css": files.css = f[i][1]; break;
		case "fjs": files.js = f[i][1]; break;
		}
	}
	return files;
}

function generateHTMLHead(htmlSource) { return htmlSource.substr(htmlSource.indexOf("<head>"), htmlSource.indexOf("</head>")) + "\n"; }

function generateHeader(files, session) {
	let out = {html: "", css: "", js: ""};
	out.html += "<div class=\"header\">\n<br>\n";

	// Generate Button HTML
	out.html += files.html.substr(files.html.indexOf("<!--headerbuttonstart-->"), files.html.indexOf("<!--headerbuttonstart-->")) + "\n";
	
	// Generate Account Panel HTML
	
	// Generate CSS

	// Generate Button js

	// Generate Account Panel js

	out.html += "</div>";
	return out;
}

run.call(this, this.files, this.session);