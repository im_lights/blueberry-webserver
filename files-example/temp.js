/*
	Auth Values
	0 = Banned - Black
	1 = Locked - Red
	2 = Reg User - Blue
	3 = Community Moderator - Green
	4 = Global Moderator - Purple
	5 = Administrator - Orange
	*/

	console.log('check for login');

	if (storedUsername) {
		console.log('stored username detected, verifying login');
		verifyLogin(storedSID, storedUserid, storedAuth, response => {
			if (response) {
				console.log('session validated');
				accountHTML =  c(storedUsername) + ' <div><button id="account" class="accountbutton"><b>Account</b></button> <button id="logout" class="accountbutton"><b>Logout</b></button>';
				if (storedAuth >= 4) accountHTML += ' <button id="staff" class="navbutton"><b>Staff Portal</b></button>';
				accountHTML += '</div>';
				$("#accountblock").css({"border": "2px solid #000000", "border-radius": "6px", "background": "-webkit-gradient(linear,left top,left bottom,from(#B8B8B8),to(#747474))"});
				$("#accountblock").html(accountHTML);

				$("#logout").click(function() {
					logout(storedSID, storedUserid, function () {
						var data = ['sid', 'userid', 'auth', 'username', 'picture'];
						for (var i = 0; i < data.length; i++) {
							if (data[i]) sessionStorage.removeItem(data[i]);
						}
						location.href = '/';
					});
				});

				$("#account").click(function() {
					verifyLogin(storedSID, storedUserid, storedAuth, response => {
						if (response) {
							location.href = '/user?uid=' + storedUserid;
						} else {
							location.href = '/login';
						}
					});
				});

				if (storedAuth >= 4) {
					$("#staff").click(function() {
						verifyLogin(storedSID, storedUserid, storedAuth, response => {
							if (response && storedAuth >= 4) {
								location.href = '/staff';
							} else {
								location.href = '/';
							}
						});
					});
				}
			} else {
				console.log('invalid session');
				$("#accountblock").html('<button id="openlogin" class="accountbutton"><b>Login</b></button> <button id="openregister" class="accountbutton"><b>Register</b></button>');

				$("#openlogin").click(function() {
					location.href = '/login';
				});

				$("#openregister").click(function() {
					location.href = '/register';
				});
			}
		});
	} else {
		$("#accountblock").html('<button id="openlogin" class="accountbutton"><b>Login</b></button> <button id="openregister" class="accountbutton"><b>Register</b></button>');

		$("#openlogin").click(function() {
			location.href = '/login';
		});

		$("#openregister").click(function() {
			location.href = '/register';
		});
	}