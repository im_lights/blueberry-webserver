"use strict";

// This file is used to set the startup and modules of the server
//
// API Documentation for build.js can be found at TODO INSERT API DOC LINK

const Build = {};

// Populate this object with your runtime modules

Build.modules = {
	webserver: [
		"./webserver", // Module Address
		"./config-example/ws", // Config
		"./files-example/index.json" // Webserver index file
	],
	loginserver: [
		"./loginserver", // Module Address
		"./config-example/ls", // Config
		"./ls/db.json" // Database Info
	],
	emailer: [
		"./emailer", // Module Address
		"./config-example/em", // Config
		"./em/info.json" // Emailer Info
	]
};

// Setup your runtime Settings

Build.settings = {
	dev: true, // Set Developer Mode
	log: true, // Set Logging Mode
	ext: true  // Management Mode - See Below
};

// Build Settings Ext controls sending of usage data from the server to
// Cherry Blue Developers for maintaining the server and reporting bugs.
// If you are maintaining the server yourself and do not want this data
// To be sent, set ext to false.

// Export to the Master Process

module.exports = Build;