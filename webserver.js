"use strict";

global.ctx = this;

class Component extends require("events") {
    constructor() {
        super();
    }
};

class HTMLC extends Component {
    constructor() {
        super();
    }
};

class CSSC extends Component {
    constructor() {
        super();
    }
};

const H = (H) => class extends HTMLC {
    constructor() {
        super();
    }
};

const C = (C) => class extends CSSC {
    constructor() {
        super();
    }
};

class Base {
    constructor() {
        
    }
};

class MultiC extends H(C(Base)) {
    constructor() {
        super();
    }
};

const classdata = {
    Component: Component,
    HTML_Component: HTMLC,
    CSS_Component: CSSC,
    Multi_Component: MultiC
};

require("./proc_wrapper")("Webserver", classdata, (key, parts) => {
	switch(key) {
	case "init":
	{
		global.Config = require(parts.shift());
		const secure = Config.ssl ? true : false;
		const fs = require("fs");
		const path = require("path");
		const url = require("url");
		const http = secure ? require("https") : require("http");

		const { Connection, ConnectionManager } = require("./modules/connection");
        const instance = new ConnectionManager();
		ConnectionManager.Upkeep(instance);

		let fileData;
		try {
			fileData = JSON.parse(fs.readFileSync(parts.shift(), "utf8"));
		} catch (e) {
            console.log(e);
			fileData = {};
		}
        
		const FileLibrary = require("./modules/file_library.js")(fileData);
        parts.shift(); // Clear out the unneeded build file
        FileLibrary.Initialize(parts.shift());

        /*
		global.sendResponse = function(res, data) {
			for (let i = 0; i < data.totalFiles; i++) {
				res.writeHead(data.files[i].code, data.files[i].type);
				res.write(data.files[i].content);
			}
			res.end();
		};
        */

		if (secure) {
			
		} else {
			http.createServer((req, res) => {
				const uri = url.parse(req.url).pathname;
				const pagepath = path.join(process.cwd(), uri);
				console.log("requested page: " + pagepath);

				const ip = req.socket.localAddress;
				const port = req.socket.localPort;
				console.log("request source: " + ip + ", port: " + port);

				const session = ConnectionManager.Request(ip, port);

				const page = FileLibrary.locate(pagepath);
				if (!page || !FileLibrary.resolve(page)) {
					FileLibrary.send404(res, session);
				} else {
					FileLibrary.generateResponse(entries, res, session);
				}
			}).listen(Config.port);
            console.log("Webserver listening on port " + Config.port);
		}
	}
	break;
	}
});

Blueberry.Inerit("Webserver");
