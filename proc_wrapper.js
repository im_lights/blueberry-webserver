"use strict";

class Blueberry {
    constructor(id, data) {
        this.id = id;
        Object.assign(this, data);
    }
    
    restart() {
        console.log("restart|");
    }
    
    kill() {
        console.log("kill|");
    }
};

function a(i, classdata, parseMessage) {
    global.Blueberry = new Blueberry(i, classdata);
	console.log("Module '" + i + "' launched.");
	process.stdin.on("data", (data) => {
        data = data.toString();
		const parts = data.split("|");
		const key = parts.shift();
		parseMessage(key, parts);
	});
}

module.exports = a;