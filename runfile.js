"use strict";

// Replace these with your own project file directories
const BuildParam = require("./build-example");
const ConfigParam = "./config-example/";
const FilesParam = "./files-example/";

const Main = new (require("./main"))(BuildParam, ConfigParam, FilesParam);