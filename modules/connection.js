"use strict";

class Login {
	constructor({sid}) {
		this.sid = sid;
	}
};

class Connection {
	constructor(ip, port) {
		this.ip = ip;
		this.port = port;
		this.created = Date.now();
		this.lastActive = Date.now();
		this.login = null;
		this.gameSession = null;
	}

	static Create(ip, port) {
		return new Connection(ip, port);
	}

	a() {
		this.lastActive = Date.now();
		return this;
	}

	Login(loginObject) {
		this.login = loginObject;
	}

	Logout() {
		this.login = null;
	}
};

class ConnectionManager {
	constructor() {
		this.connections = new Map();
		this.logins = new Map();
		this.gameSessions = new Map();
	}

	setConnection(ip, obj) {
		this.connections.set(ip, obj);
	}

	getConnection(ip) {
		return this.connections.get(ip) || null;
	}

	setLogin(id, obj) {
		this.logins.set(ip, obj);
	}

	getLogin(id) {
		return this.logins.get(id) || null;
	}

	setGameSession(sid, obj) {
		this.gameSessions.set(sid, obj);
	}

	getGameSession(sid) {
		return this.gameSessions.get(sid) || null;
	}

	// Main Methods

	Request(ip, port) {
		if (this.connections.has(ip)) {
			return this.connections.get(ip).a();
		} else {
			const c = Connection.Create(ip, port);
			this.setConnection(ip, c);
			return c;
		}
	}

	static Upkeep(instance) {
		for (const [ip, con] of instance.connections) {
			if (Date.now() - con.lastActive >= 1000 * 60 * 15 && !con.gameSession) { // 15 Minute Timeout
				// Dead Connection - Lets delete it.
				if (con.login) {
					if (instance.logins.has(con.login.userid)) {
						instance.logins.delete(con.login.userid);
					}
					con.Logout();
				}
				instance.connections.delete(ip);
			}
		}

		setTimeout(() => { // Call ourselves again in 1 minute
			this.Upkeep(instance);
		}, 1000 * 60);

		return; // Prevent a callstack overflow with recursive calls of this function
	}
};

module.exports = {
	Connection: Connection,
	ConnectionManager: ConnectionManager
};