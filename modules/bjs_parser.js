"use strict";

const Module = module.constructor;

class Parser {
	static generateOutput(files, index, session) {
		let output;
		let code = files[index][1];
		let err = false;

		try {
			output = eval.call({files: files, session: session}, code);
		} catch (e) {
			// TODO handle error
			output = null;
			console.warn("[Rewrite this handler] error in eval: " + e);
		}
        
        return output;
	}
    
    static generateContext(file, name) {
        let output = new Module();
        output._compile(file, name);
        return output;
    }
};

module.exports = Parser;