"use strict";

const fs = require("fs");
const path = require("path");
const Parser = require("./bjs_parser");

class FileLibrary {
	constructor(data) {
        this.data = data;
        
        // Uninitialized
        this.fileroot = null;
        this.runtimeContext = null;
    }
    
    Initialize(fileroot) {
        this.fileroot = fileroot;
        
        // Launch the main js file if one exists
        let mainjs = "";
        try {
            mainjs = fs.readFileSync(`${fileroot}/main.js`);
        } catch (e) {
            mainjs = null;
            this.runtimeContext = {};
        } finally {
            if (mainjs) {
                this.runtimeContext = Parser.generateContext(mainjs, "mainjs");
            }
        }
    }
    
	locate(page) { return page && this.data[page] ? this.data[page] : null; }

	resolve(entries) {
		const files = [];
		for (const i in entries) {
			files.push([i, fs.readFileSync(path.join(process.cwd(), "files", i))]);
		} 
	}

	send404(res, session) {
		let entries = this.locate("404");
		if (!entries) {
			// This only happens if you don't add a 404 page
			return null;
		} else {
			let files = this.resolve(entries);
			if (!files) {
				// TODO: handle 404 page error
				return null;
			} else {
				this.generateResponse(files, res, session, 404);
			}
		}
	}

	generateResponse(files, res, session, o = null) {
		let output = files;
		let data = { totalFiles: 0, files: [] };
		let index = null;
		for (let i = 0; i < files.length; i++) {
			if (files[i][0] == "bjs") {
				index = i;
				break;
			}
		}

		if (index) {
			// take files[index][1] and parse it as code
			output = Parser.generateOutput.call(ctx, files, index, session);
		}

		if (!output) {
			data.files.push({
				code: 500,
				type: {"Content-Type": "text/plain"},
				content: "Internal Server Error - Backend Javascript Error\n"
			});
			sendResponse(res, data);
			return;
		}

		// Output should now be ready to format
		for (const i in output) {
			data.files.push({ code: null, type: null, content: null });
			switch(output[i][0]) {
			case "html":
			{
				data.files[i].code = o || 200;
				data.files[i].type = {"Content-Type": "text/html"};
				data.files[i].content = output[i][1];
			}
			break;
			case "css":
			{
				data.files[i].code = o || 200;
				data.files[i].type = {"Content-Type": "text/css"};
				data.files[i].content = output[i][1];
			}
			break;
			case "fjs":
			{
				data.files[i].code = o || 200;
				data.files[i].type = {"Content-Type": "text/javascript"};
				data.files[i].content = output[i][1];
			}
			break;
			case "bjs": break; // Never Send Backend JS
			default:
			{
				data.files[i].code = 500;
				data.files[i].type = {"Content-Type": "text/plain"};
				data.files[i].content = "Internal Server Error - Unrecognized File Type\n";
			}
			}
		}

		sendResponse(res, data);
	}
};

function init(fileData) { return new FileLibrary(fileData); }

module.exports = init;