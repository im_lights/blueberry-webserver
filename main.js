"use strict";

class Process_API extends require("events") {
    constructor(id, data, build, files) {
        super();
        //console.log(data);
        this.id = id;
        this.name = data.shift();
        this.data = data;
        this.build = build;
        this.files = files;
        this.process = require("child_process").spawn(`node ${this.name}.js`, [1], { shell: true });
        this.processId = this.process.pid;
        this.listener = this.process.stdout.on("data", (msg) => { this.ParseMessage(msg) });
        this.err = this.process.stderr.on("data", (msg) => { console.log(msg.toString()); });
        
        // Initialize the module in the process we spawned
        this.process.stdin.write(`init|${this.data.join("|")}`);
    }
    
    ParseMessage(msg) {
        msg = msg.toString();
        if (msg.includes("|")) {
            const parts = msg.split("|");
            const key = parts.shift();
            switch (key) {
            case "err":
            {
                throw parts.join();
            }
            break;
            case "log":
            {
                if (Build.log) {
                    console.log(parts.shift());
                }
            }
            break;
            case "sqlverif": // Module is asking if sql is enabled on blackberry
            {
                if (Modules.has("MySQL")) {
                    this.process.stdin.write("sql|1");
                } else {
                    this.process.stdin.write("sql|0");
                }
            }
            break;
            case "restart":
            {
                this.process.kill();
                this.process = require("child_process").spawn(`node ${this.name}.js`, [1], { shell: true });
                this.processId = this.process.pid;
                this.listener = this.process.stdout.on("data", (msg) => { this.ParseMessage(msg) });
                this.err = this.process.stderr.on("data", (msg) => { console.log(msg.toString()); });

                // Initialize the module in the process we spawned
                this.process.stdin.write(`init|${this.data.join("|")}`);
            }
            break;
            case "kill":
            {
                this.process.kill();
                this.emit("kill");
            }
            break;
            default:
            {
                console.log(`${key}|${parts.join("|")}`);
            }
            break;
            }
        } else {
            console.log(msg);
        }
    }
}

class BlackberryMain {
    constructor(b, c, f = null) {
        require("./setup")();
        if (!b) {
            throw "Error Initializing Webserver - No build file provided.\nPlease see README.MD for more information.";
        }
        this.build = b;

        if (!c) {
            throw "Error Initializing Webserver - No Config files provided.\nPlease see README.MD for more information.";
        }
        this.config = c;
        this.files = f;
        this.crashguard = require("./crashguard");
        this.Modules = new Map(); // Module Container

        for (const i in this.build.modules) {
            try {
                this.Modules.set(...this.initModule(i, this.build.modules[i]));
            } catch (e) {
                console.log(e);
            }
        }
    }
};

function initModule(id, data) {
    let x = new Process_API(id, data, this.build, this.files);
    x.once("kill", () => { this.Modules.delete(x.processId); });
    return [x.processId, x];
}

module.exports = BlackberryMain;