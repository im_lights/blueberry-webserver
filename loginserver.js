"use strict";

global.ctx = this;
global.queryCallback = function(){};

class SQL {
    constructor() {}
    
    static Verify() {
        process.send("sqlverif|");
    }
    
    static Query(str, callback) {
        process.send(`sqlquery|${str}`);
        global.queryCallback = callback;
    }
};

function isEmail(str) {
    const emailhosts = fs.readFileSync("./config/emailhosts.csv", "utf8").split(',');
    let res = false;
    for (const i of emailhosts) {
        if (str.includes(i)) {
            res = true;
            break;
        }
    }
    return res;
}

function findUser(username, email) {
    
}

function handler(req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/plain',
		'Access-Control-Allow-Origin': '*'
	});
    req.on("data", (data) => {
        data = data.toString();
        let reply = ""; // Will be changed by interpretter
        const key = data.charAt(0);
        const parts = data.split("...");
        const params = parts[1].split('|');
        switch(key) {
        case "l": // Login
        {
            let a0 = null; // Username
            let a1 = null; // Email
            
            if (isEmail(params[0])) {
                a1 = params[0];
            } else {
                a0 = params[0];
            }
            findUser(a0, a1, (err, userid) => {
                if (err) {
                    reply = "err";
                    throw err;
                } else if (!userid) {
                    reply = "cunouser";
                } 
            });
        }
        break;
        }
        res.end();
    });
}

require("./proc_wrapper")("Loginserver", {}, (key, parts) => {
	switch(key) {
	case "init":
	{
		global.Config = require(parts.shift());
		const secure = Config.ssl ? true : false;
		const fs = require("fs");
		const path = require("path");
		const http = secure ? require("https") : require("http");

		global.sendResponse = (res, data) => {
			for (let i = 0; i < data.totalFiles; i++) {
				res.writeHead(data.files[i].code, data.files[i].type);
				res.write(data.files[i].content);
			}
			res.end();
		};
        const server = secure ?
            http.createServer({ca: Config.ssl.ca, cert: Config.ssl.cert, key: Config.ssl.key}, (req, res) => handler(req, res)) :
            http.createServer((req, res) => handler(req, res)).listen(Config.port);
        
        console.log("Loginserver listening on port " + Config.port);
	}
    case "sql": // Response to sqlverif
    {
        queryCallback(parts[0] == "1" ? true : false);
        queryCallback = function(){};
    }
	break;
	}
});